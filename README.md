
# react-native-beacon-advertiser

## Getting started

`$ npm install react-native-beacon-advertiser --save`

### Mostly automatic installation

`$ react-native link react-native-beacon-advertiser`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-beacon-advertiser` and add `RNBeaconAdvertiser.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNBeaconAdvertiser.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNBeaconAdvertiserPackage;` to the imports at the top of the file
  - Add `new RNBeaconAdvertiserPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-beacon-advertiser'
  	project(':react-native-beacon-advertiser').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-beacon-advertiser/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-beacon-advertiser')
  	```

## Usage
```javascript
import RNBeaconAdvertiser from 'react-native-beacon-advertiser';
```
