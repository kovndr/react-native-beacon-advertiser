
package com.reactlibrary;

import android.content.Context;
import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RNBeaconAdvertiserModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNBeaconAdvertiserModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "RNBeaconAdvertiser";
  }

  @ReactMethod
  public void startAdvertising(String message, int major, int minor, String uuid) {
//    Toast.makeText(getReactApplicationContext(), message + "ma:" + major + " mi:" + minor+ " uuid:" + uuid, Toast.LENGTH_LONG).show();
    startAdvertising(major, minor, uuid);
  }
  @ReactMethod
  public void stopAdvertising(String message) {
//    Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_LONG).show();
    stopAdvertising();
  }

  private static Intent getServiceIntent(Context c) {
    return new Intent(c, AdvertiserService.class);
  }

  /**
   * Starts BLE Advertising by starting {@code AdvertiserService}.
   */
  private void startAdvertising(int major, int minor, String uuid) {
    Context c = getCurrentActivity();
    Intent i = getServiceIntent(c);
    i.putExtra("major", major);
    i.putExtra("minor", minor);
    i.putExtra("uuid", uuid);
    if (c != null) {
      c.startService(i);
    }
  }

  /**
   * Stops BLE Advertising by stopping {@code AdvertiserService}.
   */
  private void stopAdvertising() {
    Context c = getCurrentActivity();
    if (c != null) {
      c.stopService(getServiceIntent(c));
    }
  }
}