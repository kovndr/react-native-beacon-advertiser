
Pod::Spec.new do |s|
  s.name         = "RNBeaconAdvertiser"
  s.version      = "1.0.0"
  s.summary      = "RNBeaconAdvertiser"
  s.description  = <<-DESC
                  RNBeaconAdvertiser
                   DESC
  s.homepage     = "https://github.com/objectionlabs/react-native-beacon-advertiser"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/objectionlabs/react-native-beacon-advertiser.git", :tag => "master" }
  s.source_files  = "RNBeaconAdvertiser/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

