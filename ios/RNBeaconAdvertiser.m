
//#import "RNBeaconAdvertiser.h"
//#import <React/RCTLog.h>

//@implementation RNBeaconAdvertiser
//
////- (dispatch_queue_t)methodQueue
////{
////    return dispatch_get_main_queue();
////}
//RCT_EXPORT_MODULE()
//
//RCT_EXPORT_METHOD(startAdvertising:(NSString *)message major:(NSNumber *)major minor:(NSNumber *)minor)
//{
//    RCTLogInfo(@"Start ADVERT %@ - %@ : %@", message, major, minor);
//    NSLog(@"Start ADVERT ###### %@ - %@ : %@", message, major, minor);
//}
//
//@end
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNBeaconAdvertiser, NSObject)

RCT_EXTERN_METHOD(startAdvertising:(NSString *)message major:(nonnull NSNumber *)major minor:(nonnull NSNumber *)minor uuid:(NSString *)uuid)
RCT_EXTERN_METHOD(stopAdvertising:(NSString *)message)

@end

